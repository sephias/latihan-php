<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h2>SOAL NO 1</h2>
    <?php
        $first_sentence = "Hello PHP!";
        echo "Kalimat pertama : ". $first_sentence . "<br>";
        echo "Panjang String : " . strlen($first_sentence) . "<br>";
        echo "Jumlah Kata : " . str_word_count($first_sentence) ."<br><br>";

        $second_sentence = "I'm ready for the challenges";
        echo "Kalimat pertama : ". $second_sentence . "<br>";
        echo "Panjang String : " . strlen($second_sentence) . "<br>";
        echo "Jumlah Kata : " . str_word_count($second_sentence) . "<br><br>";
    
        echo "<h3>SOAL NO 2</h3>";
        $string2 = "I love PHP";
        echo "Kalimat kedua : " . $string2."<br>";
        echo "Kata pertama : " . substr($string2,0,1) . "<br>";
        echo "Kata kedua : " . substr($string2,2,4) . "<br>";
        echo "Kata ketiga : " . substr($string2,7,3);

        echo "<h3>SOAL NO 3</h3>";
        $string3 = "PHP is old but Good!";
        echo "Kalimat ketiga : " . $string3. "<br>";
        echo "Ganti Kalimat ketiga : " . str_replace("Good!","Awesome",$string3);

    ?>
</body>
</html>

